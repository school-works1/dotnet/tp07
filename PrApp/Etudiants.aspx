﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Etudiants.aspx.cs" Inherits="PrApp.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style2 {
            width: 100%;
            margin-top: 0px;
            height: 28px;
        }
        .auto-style1 {
            width: 201px;
        }
        .auto-style3 {
            width: 188px;
        }
        .auto-style4 {
            width: 201px;
            height: 29px;
        }
        .auto-style5 {
            width: 188px;
            height: 29px;
        }
        .auto-style6 {
            height: 29px;
        }
        .auto-style7 {
            margin-left: 107px;
        }
        .auto-style8 {
            margin-left: 71px;
        }
        .auto-style9 {
            font-size: x-large;
        }
        .auto-style10 {
            text-align: center;
            height: 45px;
        }
        .auto-style11 {
            height: 374px;
            margin-top: 60px;
        }
        .auto-style12 {
            font-size: large;
            width: 383px;
            margin-top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="auto-style10">

            <strong><span class="auto-style9">Ajout / Modification / Suppression d&#39;étudiants<br />
            </span></strong>
            <div class="auto-style12">
                Liste des étudiants</div>
            <br />
            <br />

        </div>
        <div class="auto-style11">

            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="CIN" DataSourceID="SqlDataSource1">
                <Columns>
                    <asp:BoundField DataField="Nom" HeaderText="Nom" SortExpression="Nom" />
                    <asp:BoundField DataField="CIN" HeaderText="CIN" ReadOnly="True" SortExpression="CIN" />
                    <asp:BoundField DataField="Classe" HeaderText="Classe" SortExpression="Classe" />
                </Columns>
            </asp:GridView>

            <br />
        <table class="auto-style2">
            <tr>
                <td class="auto-style1">CIN</td>
                <td class="auto-style3">
                    <asp:TextBox ID="TextBox1" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
&nbsp; </td>
                <td>&nbsp;&nbsp; </td>
            </tr>
            <tr>
                <td class="auto-style4">Nom</td>
                <td class="auto-style5">
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                </td>
                <td class="auto-style6"></td>
            </tr>
            <tr>
                <td class="auto-style4">Classe</td>
                <td class="auto-style5">
                    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDSClasse" DataTextField="Nom" DataValueField="Code" Height="19px" Width="168px">
                    </asp:DropDownList>
                </td>
                <td class="auto-style6">
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                </td>
            </tr>
        </table>
            <br />
            <br />
            <asp:Button ID="ButtonAdd" runat="server" OnClick="Button1_Click" Text="Ajouter" Width="87px" />
            <asp:Button ID="Button1" runat="server" CssClass="auto-style7" OnClick="Button1_Click1" Text="Modifier" />
            <asp:Button ID="Button2" runat="server" CssClass="auto-style8" OnClick="Button2_Click" Text="Supprimer" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ENNOURI ABDELOUAHED<br />
            <br />
        </div>
        <p>
            &nbsp;</p>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ScolaritConnectionString %>" DeleteCommand="DELETE FROM [Etudiant] WHERE [CIN] = @CIN" InsertCommand="INSERT INTO [Etudiant] ([Nom], [CIN], [Classe]) VALUES (@Nom, @CIN, @Classe)" SelectCommand="SELECT Etudiant.Nom, Etudiant.CIN, Classe.Nom as Classe FROM Etudiant INNER JOIN Classe ON Etudiant.Classe = Classe.Code" UpdateCommand="UPDATE [Etudiant] SET [Nom] = @Nom, [Classe] = @Classe WHERE [CIN] = @CIN">
            <DeleteParameters>
                <asp:ControlParameter ControlId="TextBox1" Name="CIN" Type="String"/>
            </DeleteParameters>
            <InsertParameters>
                <asp:ControlParameter ControlId="TextBox2" Name="Nom" Type="String" />
                <asp:ControlParameter ControlId="TextBox1" Name="CIN" Type="String" />
                <asp:ControlParameter ControlId="DropDownList1" Name="Classe" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:ControlParameter ControlId="TextBox2" Name="Nom" Type="String" />
                <asp:ControlParameter ControlId="DropDownList1" Name="Classe" Type="String" />
                <asp:ControlParameter ControlId="TextBox1" Name="CIN" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDSClasse" runat="server" ConnectionString="<%$ ConnectionStrings:ScolaritConnectionString %>" SelectCommand="SELECT * FROM [Classe]"></asp:SqlDataSource>
    </form>
</body>
</html>
